# NLP_REST #

This project trains a text classifier, pickles the model of interest, and deploys it on the web as 
a REST API using Flask RESfull.

* The training data can be found here: https://www.kaggle.com/soumikrakshit/yahoo-answers-dataset

### Structure of the project: ###
NLP_REST/  
----├── README.md  
----├── app.py    
----├── build_model.py    
----├── model.py  
----├── NLP_model_dev.py  
----└── lib/  
--------├── QAClassifier.pkl  
--------└── TFIDFVectorizer.pkl  

#### NLP_model_dev ####
* An exploratory analysis to find out what classifier, with what parameters, fits the data best.

#### model ####
* Using the result from the NLP_model_dev, defines the structure of the classifier of interest

#### build_model ####
* Using the actual data, runs the defined model and trains a classifier (plus the TF-IDF vectorizer), and generate the pickle files

#### app.py ####
* Loads and runs the pickles on a local host 

#### lib ####
* Contains the pickle files of the TF-IDF vectorizer and the text classier. Both of the files are needed to run the model and query the classifier. 

### How to query the model? ###

The trained model is live and accessible over the web. The model is deployed as a REST API using Flask RESTful

To access the web application and query the trained model, the input should be in the format of the command below and submitted via http:


```
curl -X GET http://hosseinmhz.pythonanywhere.com/ -d title="title here" -d content="content here" -d answer="and answer here"
```
As a response, a JSON object will be return in the format below:  
```
{"Prediction": "Class ID", "Category": "Class Label"} 
```
#### Example:####
Query:  
```
curl -X GET http://hosseinmhz.pythonanywhere.com/ -d title="why is the sky blue?" -d content="" -d answer="It's because sunlight striking air molecules is scattered in all directions."  
```
  
Answer:  
```
{"Prediction": "2", "Category": "Science & Mathematics"}  
```


### Python libraries: ###
* Pandas
* Numpy
* Sklearn: TfidfVectorizer, MultinomialNB, SVC, SGDClassifier, MLPClassifier, GridSearchCV, RandomForestClassifier, chi2, SelectKBest, Pipeline, metrics
* Pickle
* Flask
* Flask_restful
* Matplotlib.pyplot