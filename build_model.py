# -*- coding: utf-8 -*-
"""
Created on Sat May 16 22:11:45 2020

@author: Hosein
"""

from model import QACLFModel
from sklearn.feature_selection import chi2, SelectKBest
import pandas as pd


def build_model():
    model_clf = QACLFModel()

    columns_header = ["label", "title", "content", "answer"] # columns header for the dataframe
    trainset = pd.read_csv("../yahoo_answers_csv/train.csv", header = None)
    trainset.columns = columns_header


    # Removing the rows with missing values in the column Answer
    trainset = trainset[trainset['answer'].notna()] 
    
    # Creating a new column (feature) by combining two filed: Title and Answer
    trainset['title_answer'] = trainset['title'] + trainset['answer']

    #train_tfidf_vec = model.vectorizer_fit_transform(trainset['title_answer'])
    model_clf.vectorizer_fit(trainset['title_answer'])
    train_tfidf_vec = model_clf.vectorizer_transform(trainset['title_answer'])
    
    #ch2_selection = SelectKBest(chi2, k=500)
    #train_tfidf_vec = ch2_selection.fit_transform(train_tfidf_vec, trainset['label'])

    print('Vectorizer fit transform complete')


    model_clf.train(train_tfidf_vec, trainset['label'])
    print('Model training complete')

    
    model_clf.pickle_model()
    model_clf.pickle_vectorizer()
    
    print("test pred")
    uq_vectorized = model_clf.vectorizer_transform(["What makes friendship click"])
    pred = model_clf.predict(uq_vectorized)
    print(pred)


if __name__ == "__main__":
    build_model()