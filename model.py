# -*- coding: utf-8 -*-
"""
Created on Sat May 16 21:10:14 2020

@author: Hosein
"""

from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle

class QACLFModel(object):

    def __init__(self):
        self.model = MultinomialNB(alpha = 0.01, fit_prior = True)
        #self.tfidf_vectorizer = TfidfVectorizer()
        self.tfidf_vectorizer = TfidfVectorizer(max_df = 0.5, min_df = 10, stop_words = "english", 
                                                  lowercase = True, ngram_range = (1,2),
                                                  max_features = 25000,
                                                  token_pattern= u'(?ui)\\b\\w*[a-z]+\\w*\\b')
#   Fits a tfidf vectorizer to the corpus            
    def vectorizer_fit(self, X):
        self.tfidf_vectorizer.fit(X)

#   Transforms the text data to a tfidf matrix
    def vectorizer_transform(self, X):
        X_transformed = self.tfidf_vectorizer.transform(X)
        return X_transformed
    
#   Trains the classifier
    def train(self, X, y):
        self.model.fit(X, y)

#   Returns the prediction based on the trained classifier
    def predict(self, X):
        y_pred = self.model.predict(X)
        return y_pred

#   Pickles the tfidf vectorizer
    def pickle_vectorizer(self, path='TFIDFVectorizer.pkl'):
        with open(path, 'wb') as f:
            pickle.dump(self.tfidf_vectorizer, f)
            print("Pickled the vectorizer")

#   Pickles the classifier
    def pickle_model(self, path='QAClassifier.pkl'):
        with open(path, 'wb') as f:
            pickle.dump(self.model, f)
            print("Pickled the classifier")

