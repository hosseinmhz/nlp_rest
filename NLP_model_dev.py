# -*- coding: utf-8 -*-
"""
Created on Sun May 15 12:52:20 2020

@author: Hosein
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import chi2, SelectKBest
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix


""" ***** Data preparation, data analysis and prepresoosing and model training  *****"""

# Step 0: Data preparation 
columns_header = ["label", "title", "content", "answer"] # column names for the dataframe
trainset = pd.read_csv("../yahoo_answers_csv/train.csv", header = None)
trainset.columns = columns_header #setting the columns names
testset = pd.read_csv("../yahoo_answers_csv/test.csv", header = None)
testset.columns = columns_header #setting the columns names

# Removing the rows with missing values in the column Answer
trainset = trainset[trainset['answer'].notna()]
testset = testset[testset['answer'].notna()]

# Creating a new feature by combining two filed: Title and Answer
""" Combination of the two fileds may lead to a better performance """
trainset['title_answer'] = trainset['title'] + trainset['answer']
testset['title_answer'] = testset['title'] + testset['answer']


# **** Data is balanced - train: 140,000 of each class, test: 6,000 of each class

# Step 1: Feature extraction and data wrangling
tfidf_vectorizer = TfidfVectorizer(max_df = 0.5, min_df = 10, stop_words = "english", 
                            lowercase = True, ngram_range = (1,2), # considering both 1 grams and 2 grams
                            max_features = 5000, # it will keep top 5000 terms based on their term frequency
                            token_pattern= u'(?ui)\\b\\w*[a-z]+\\w*\\b') # ignore the terms that includes only number
train_tfidf_vec = tfidf_vectorizer.fit_transform(trainset['title_answer'])
train_features = tfidf_vectorizer.get_feature_names()

test_tfidf_vec = tfidf_vectorizer.transform(testset['title_answer'])
test_features = tfidf_vectorizer.get_feature_names()


# Step 2: Exploring 4 classifiers to get insight of the trainability of the data and choose the best model

#SVM
classification(SGDClassifier(), train_tfidf_vec, trainset['label'], test_tfidf_vec, testset['label'])

# NaiveBayes
classification(MultinomialNB(alpha = 0.01, fit_prior = True), train_tfidf_vec, trainset['label'], test_tfidf_vec, testset['label'])

### test on single text
test_x = tfidf_vectorizer.transform(["Why do women get PMS"])
model = MultinomialNB()
model.fit(train_tfidf_vec, trainset['label'])
model.predict(test_x)
#####

# Random Forest
classification(RandomForestClassifier(max_depth=10, random_state=0), train_tfidf_vec, trainset['label'], test_tfidf_vec, testset['label'])

# NN
model_NN = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5,), random_state=1,
                         activation = 'relu', max_iter=50)
classification(model_NN, train_tfidf_vec, trainset['label'], test_tfidf_vec, testset['label'])

""" Which clssifier is the best?
# Result: 4 classifier were trained over 3 different combinations of the data (using top 5000 tf values):
#         (i) title only, (ii) answer only, (iii) title and answer 
#         Overall, all the models have the best perfromance when both title and answer are considered,
#         and lowest performance when answer only (from 5 to 10 percent improvement when considered both fileds).
#         Random Forest has the lowest performance with (acc: 27%, f1: 28%) in title only growing up to 39% for oth accuracy and F1.
#         The other 3 classifier have very similair performance around 60% in title only and 65% in title and answer together.
#         Overall SVM and NN do slightly better than NB, but SVM takes secodns to train, NN around 10 minutes
#         So -> NB it is
#         * NB over whole data (not limited to top 5000) -> accuracy and f1 of 70% and 69% respectively.   
"""

# Step 3: Feature engineering -> Dimensionality reduction 
# generate the TFIDF matrix again, this time with all the phrases inlcuded
""" Using chi2 score to find the most relevant terms that help boost the classifer """
tfidf_vectorizer = TfidfVectorizer(max_df = 0.5, min_df = 10, stop_words = "english", 
                            lowercase = True, ngram_range = (1,2),
                            token_pattern= u'(?ui)\\b\\w*[a-z]+\\w*\\b')
train_tfidf_vec = tfidf_vectorizer.fit_transform(trainset['title_answer'])
train_features = tfidf_vectorizer.get_feature_names()

test_tfidf_vec = tfidf_vectorizer.transform(testset['title_answer'])
test_features = tfidf_vectorizer.get_feature_names()

# Sensitivity analysis: try k = 5000, 10000, 25000, 50000, 
# and 160,000 (based on chi2 cutoff threshold - statistical signficance)
ch2_selection = SelectKBest(chi2, k=25000)
train_chi2_selected = ch2_selection.fit_transform(train_tfidf_vec, trainset['label'])
test_chi2_selected = ch2_selection.transform(test_tfidf_vec)
classification(MultinomialNB(), train_chi2_selected, trainset['label'], test_chi2_selected, testset['label'])

"""
# Result: k = 25,000 has the best results: 
# only 1% decrease in accuracy and 2% decrease in f1, comparing with the model that
# includes total dataset with 637,006 terms (features)
# compared with other models, NB is still the best
"""

# Step 4: hyperparameter tuning

model = Pipeline([('clf', MultinomialNB())])
#model = MultinomialNB()
model.fit(train_chi2_selected, trainset['label'])
prediction = model.predict(test_chi2_selected)
np.mean(predicted ==  testset['label'])

parameters = {'clf__fit_prior': (True, False), 'clf__alpha': (2.0, 1.0, 0.1, 0.01, 0.001)}
model_hp_tuning = GridSearchCV(model, parameters, cv=5, n_jobs=-1)
model_hp_tuning = model_hp_tuning.fit(train_chi2_selected, trainset['label'])
model_hp_tuning.best_score_ # best score is 69%
for param_name in sorted(parameters.keys()):
    print("%s: %r" % (param_name, model_hp_tuning.best_params_[param_name]))
# best parameters are: clf__alpha: 0.01, and clf__fit_prior: True

"""
# ******* Final configuration ********* -> will be used for the pickling and RestAPI
# Data: 
#       input = title + answer
#       TFIDF = with 2 grams
#       TFIDF_vector = top 25,000 terms based on chi2 score
# Classifier:
#       MultinomialNB(alpha = 0.01, fit_prior = True)
"""

# additional methods
# To measure the performance of a classifier using Accuracy and F1 Score 
def performance_metrics(actual, prediction):
    accuracy = accuracy_score(actual, prediction)
    precision = precision_score(actual, prediction, average = 'macro') #Since the data is well-balances I use Macro to calculate the mean of performance metrics
    recall = recall_score(actual, prediction, average = 'macro')
    f1 = f1_score(actual, prediction, average = 'macro')
    # I conider accuracy and f1 to measure the performance.  
    return accuracy, f1 

# To run a classifier model with the passing arguemnts (data input for train and test) 
def classification(model, train_X, train_Y, test_X, test_Y):
    model.fit(train_X, train_Y)
    prediction = model.predict(test_X)
    #model.predict_proba(test_X)
    print(performance_metrics(test_Y, prediction))
    
    
# Calculations of chi2 to find the cutoff threshold
chi2score_ = chi2(train_tfidf_vec, trainset['label'])[0]
train_tfidf_chi2_vec = zip(train_features, chi2score_)
train_tfidf_chi2_vec = sorted(train_tfidf_chi2_vec, key=lambda x:x[1])
top_train_tfidf_chi2 = list(zip(*train_tfidf_chi2_vec[-159006:]))
#159006

labels = top_train_tfidf_chi2[0]
plt.figure(figsize=(15,10))
x = range(len(top_train_tfidf_chi2[1]))
plt.barh(x,top_train_tfidf_chi2[1], align='center', alpha=0.2)
plt.plot(top_train_tfidf_chi2[1], x, '-o', markersize=5, alpha=0.8)
plt.yticks(x, labels)
plt.xlabel('$\chi^2$')