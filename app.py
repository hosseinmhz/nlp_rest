# -*- coding: utf-8 -*-
"""
Created on Sat May 16 22:18:10 2020

@author: Hosein
"""

from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import pickle
import numpy as np
from model import QACLFModel
import codecs, json

app = Flask(__name__)
api = Api(app)


model = QACLFModel()
classes = ["Society & Culture", "Science & Mathematics", "Health", "Education & Reference",
            "Computers & Internet", "Sports", "Business & Finance", "Entertainment & Music",
             "Family & Relationships", "Politics & Government"]

vec_path = 'lib/TFIDFVectorizer.pkl'
with open(vec_path, 'rb') as f:
    model.tfidf_vectorizer = pickle.load(f)

clf_path = 'lib/QAClassifier.pkl'
with open(clf_path, 'rb') as f:
    model.model = pickle.load(f)

# argument parsing
parser = reqparse.RequestParser()
parser.add_argument('title')
parser.add_argument('content')
parser.add_argument('answer')


class QAClassifier(Resource):
    def get(self):
        # use parser and find the user's query
        args = parser.parse_args()
        doc_title = args['title']
        doc_content = args['content']
        doc_answer = args['answer']
        doc_text = str(doc_title) + " " + str(doc_answer)
        print("input is: ", doc_text)
        # vectorize the user's query and make a prediction
       
        uq_vectorized = model.vectorizer_transform([doc_text])
        
        prediction = model.predict(uq_vectorized)
        
        # create JSON object
        #output = {'prediction': prediction,}
        pred_class_no = str(prediction[0])
        pred_class_title = str(classes[prediction[0]-1])
        print("The input belongs to categry:", classes[prediction[0]-1], " (Label[", prediction[0], "])")  
        output = {'Prediction': pred_class_no, 'Category': pred_class_title, }
        return output


# Setup the Api resource routing here
# Route the URL to the resource
api.add_resource(QAClassifier, '/')


if __name__ == '__main__':
    app.run(debug=False)